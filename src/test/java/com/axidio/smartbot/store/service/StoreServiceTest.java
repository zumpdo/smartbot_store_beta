package com.axidio.smartbot.store.service;

import static com.axidio.smartbot.store.utils.TestUtils.prepareStore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.bson.types.ObjectId;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.axidio.smartbot.store.model.Store;
import com.axidio.smartbot.store.repository.StoresRepository;;

@RunWith(MockitoJUnitRunner.class)
public class StoreServiceTest {

	@Mock
	private StoresRepository storeRepository;

	@InjectMocks
	private StoresService storeService;

	@Test
	public void testCreateStoreForValidStore() {
		
		//returns a mock store object when save method is invoked in storerepository
//		when(storeRepository.save(ArgumentMatchers.<Store>any())).thenReturn(prepareStore());
		
		Store store = storeService.createStore(prepareStore());
//		storeService.deleteStore(store.getIdAsObjectId());
		System.out.println("STORE ID-----" + store.getId());
		//To assert if id is random and not equal to 0.
		assertNotEquals(0, store.getId());
		
		//To verify if save method in storeRepository is invoked.
		verify(storeRepository).save(ArgumentMatchers.<Store>any());

	}
	
	@Test
	public void testCreateStoreForValidStore2() {
		
		//returns a mock store object when save method is invoked in storerepository
		when(storeRepository.save(ArgumentMatchers.<Store>any())).thenReturn(prepareStore());
		
		Store store = storeService.createStore(prepareStore());
//		storeService.deleteStore(store.getIdAsObjectId());
		System.out.println("STORE ID-----" + store.getId());
		//To assert if id is random and not equal to 0.
		assertNotEquals(0, store.getId());
		
		//To verify if save method in storeRepository is invoked.
		verify(storeRepository).save(ArgumentMatchers.<Store>any());

	}
	
//	
//	@Test
//	public void testGetStoreByIdForValidStore() {
//		
//		//returns a mock store object when save method is invoked in storerepository
////		when(storeRepository.save(ArgumentMatchers.<Store>any())).thenReturn(prepareStore());
//		
//		Store store = storeService.createStore(prepareStore());
//		
//		System.out.println("Created store --- " + store);
//		System.out.println("Created Object ID --- " + store.getIdAsObjectId()); 
//		Optional<Store> created = storeService.getStoreById(store.getIdAsObjectId());
//		System.out.println("Created store as optional--- " + created);
//		Store retrieved = storeService.getStoreById(store.getIdAsObjectId()).get();
//		
//		System.out.println(retrieved);
//		storeService.deleteStore(store.getIdAsObjectId());
//		
//		//To assert if id is random and not equal to 0.
//		assertNotEquals(0, retrieved.getId());
//		
//		//To verify if save method in storeRepository is invoked.
//		verify(storeRepository).save(ArgumentMatchers.<Store>any());
//
//	}
//	
//	@Test(expected = NoSuchElementException.class)
//	public void testDeleteStoreForValidStore() {
//		
//		//returns a mock store object when save method is invoked in storerepository
////		when(storeRepository.save(ArgumentMatchers.<Store>any())).thenReturn(prepareStore());
//		
//		Store store = storeService.createStore(prepareStore());
//		storeService.deleteStore(store.getIdAsObjectId());
//		Store retrieved = storeService.getStoreById(store.getIdAsObjectId()).get();
//		
//		//To verify if save method in storeRepository is invoked.
//		verify(storeRepository).save(ArgumentMatchers.<Store>any());
//
//	}
//	
//	
//
//	@Test
//	public void testGetStoreByNumberForValidStore() {
//		
//		//returns a mock store object when save method is invoked in storerepository
////		when(storeRepository.save(ArgumentMatchers.<Store>any())).thenReturn(prepareStore());
//		
//		Store store = storeService.createStore(prepareStore());
//		
//		System.out.println("testGetStoreByNumberForValidStore = Created store --- " + store);
//		
//		Store retrieved = storeService.getStoreByNumber(store.getNumber()).get();
//		storeService.deleteStore(store.getIdAsObjectId());
//		
//		//To assert if id is random and not equal to 0.
//		assertNotEquals(0, retrieved.getId());
//		
//		//To verify if save method in storeRepository is invoked.
//		verify(storeRepository).save(ArgumentMatchers.<Store>any());
//
//	}
//	
	
	
//	
	@Test(expected = NullPointerException.class)
	public void testGetStoresForNullStore() {
		storeService.createStore(null);
		//To verify if save method in storeRepository is invoked.
		verify(storeRepository).save(ArgumentMatchers.<Store>any());

	}
	
	
	@Test
    public void testGetByIdForValidObjectId() {
        final Store store = prepareStore();
        when(storeRepository.findById(ArgumentMatchers.<ObjectId>any())).thenReturn(Optional.of(store));
        assertEquals(store, storeService.getStoreById(ObjectId.get()).get());
        verify(storeRepository).findById(ArgumentMatchers.<ObjectId>any());
    }
	
	@Test(expected = NoSuchElementException.class)
    public void testDeleteStoreForInvalidObjectId() {
		  when(storeRepository.findById(ArgumentMatchers.<ObjectId>any())).thenThrow(new NoSuchElementException());
		storeService.deleteStore(ObjectId.get());
        verify(storeRepository).findById(ArgumentMatchers.<ObjectId>any());
    }


	@Test
    public void testGetStoreByNumberForValidStoreNumber() {
        final Store store = prepareStore();
        when(storeRepository.findByNumber(ArgumentMatchers.<String>any())).thenReturn(Optional.of(store));
        assertEquals(store, storeService.getStoreByNumber("asd").get());
        verify(storeRepository).findByNumber(ArgumentMatchers.<String>any());
    }
	
	@Test
    public void testModelForToString() {
        final Store store = prepareStore();
        assertNotNull(store.toString());
    }

}

package com.axidio.smartbot.store.controller;

import static com.axidio.smartbot.store.utils.TestUtils.prepareStore;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.axidio.smartbot.store.model.Store;
import com.axidio.smartbot.store.service.StoresService;
import com.axidio.smartbot.store.enums.OrderBy;
import com.axidio.smartbot.store.exceptions.InvalidSortOrderException;
import com.axidio.smartbot.store.enums.Direction;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StoreControllerTest {

	private static final String BASE_STORE_URL = "/stores/";

	@Autowired
	protected MockMvc mvc;

	@Autowired
	private StoresService storeService;

	@Before
	public void before() {
		storeService.deleteAll();
	}

	@After
	public void after() {
		storeService.deleteAll();
	}

	@Test
	public void testCreateStoreForValidStore() throws Exception {
		final Store store = prepareStore();
		mvc.perform(post(BASE_STORE_URL).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new Gson().toJson(store).getBytes()))
				// .andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", notNullValue()));
	}

	@Test
	public void testCreateStoreForNullStore() throws Exception {
		mvc.perform(post(BASE_STORE_URL).contentType(MediaType.APPLICATION_JSON_UTF8).content(""))
				// .andExpect(status().isCreated())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetStoreById() throws Exception {
		final Store store = storeService.createStore(prepareStore());
		mvc.perform(get(BASE_STORE_URL + "/" + store.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", is(store.getId())));
	}

	@Test
	public void testGetStoreByIdForInvalidId() throws Exception {
		mvc.perform(get(BASE_STORE_URL + "/" + "1234")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testGetStoreByIdForInvalidObjectId() throws Exception {
		mvc.perform(get(BASE_STORE_URL + "/" + ObjectId.get())).andExpect(status().isNotFound());
	}

	@Test
	public void testGetStoreByNumberForRandomNumber() throws Exception {
		mvc.perform(get(BASE_STORE_URL + "/get/" + ObjectId.get())).andExpect(status().isNotFound());
	}

	@Test
	public void testGetStoreByNumberForValidNumber() throws Exception {
		final Store store = storeService.createStore(prepareStore());
		mvc.perform(get(BASE_STORE_URL + "/get/" + store.getNumber())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", is(store.getId())));
	}
	

	@Test
	public void testDeleteStoreByValidId() throws Exception {
		final Store store = storeService.createStore(prepareStore());
		mvc.perform(delete(BASE_STORE_URL + "/" + store.getId())).andExpect(status().isOk());
		mvc.perform(get(BASE_STORE_URL + "/" + store.getId())).andExpect(status().isNotFound());
	}
	
	@Test
	public void testDeleteStoreByInvalidObjectId() throws Exception {
		mvc.perform(delete(BASE_STORE_URL + "/" + "246456")).andExpect(status().isBadRequest());
	}
	


	@Test
	public void testModifyStoreForValidStore() throws Exception {
		final Store store = storeService.createStore(prepareStore());
		store.setName("Modified");
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("id", store.getId());
		mvc.perform(put(BASE_STORE_URL + "/"+ store.getId()) //.params(paraMap)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new Gson().toJson(store).getBytes()))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(jsonPath("$.name", is("Modified")));
	}
	

	@Test
	public void testModifyStoreForNullStore() throws Exception {
		final Store store = storeService.createStore(prepareStore());
		store.setName("Modified");
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("id", store.getId());
		mvc.perform(put(BASE_STORE_URL + "/"+ store.getId()) //.params(paraMap)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(""))
		.andExpect(status().isBadRequest());		
	}
	
	@Test
	public void testGetStoresByValidParams() throws Exception {
		
		final Store store = prepareStore();
		mvc.perform(post(BASE_STORE_URL).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new Gson().toJson(store).getBytes()));
				// .andExpect(status().isCreated())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//				.andExpect(jsonPath("$.id", notNullValue()));
		
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("orderBy", OrderBy.NUMBER.getOrderByCode().toString());
	    paraMap.add("direction", Direction.ASCENDING.getDirectionCode().toString());
	    paraMap.add("page", "0");
	    paraMap.add("size", "5");
	    
		mvc.perform(get(BASE_STORE_URL).params(paraMap))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.content", hasSize(1)));
	}
	
	@Test
	public void testGetStoresByDescendingOrder() throws Exception {
		
		final Store store = prepareStore();
		mvc.perform(post(BASE_STORE_URL).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(new Gson().toJson(store).getBytes()));
				// .andExpect(status().isCreated())
//				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//				.andExpect(jsonPath("$.id", notNullValue()));
		
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("orderBy", OrderBy.NUMBER.getOrderByCode().toString());
	    paraMap.add("direction", Direction.DESCENDING.getDirectionCode().toString());
	    paraMap.add("page", "0");
	    paraMap.add("size", "5");
	    
		mvc.perform(get(BASE_STORE_URL).params(paraMap))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.content", hasSize(1)));
	}
	
	@Test (expected = InvalidSortOrderException.class)
	public void testGetStoresByInvalidDirection() throws Exception {
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("orderBy", OrderBy.NUMBER.getOrderByCode().toString());
	    paraMap.add("direction", "InvalidDirection");
	    paraMap.add("page", "0");
	    paraMap.add("size", "5");
	    
	    try {
		mvc.perform(get(BASE_STORE_URL).params(paraMap))
		.andExpect(status().is5xxServerError())
		.andExpect(content().string("Invalid sort direction"));
	    } catch(Exception invalidSortOrderException) {
	    	throw (Exception) invalidSortOrderException.getCause();
	    }
				
	}
	
	
	@Test (expected = InvalidSortOrderException.class)
	public void testGetStoresByInvalidOrderColumn() throws Exception {
		MultiValueMap<String, String> paraMap =new LinkedMultiValueMap<>();
	    paraMap.add("orderBy", OrderBy.NUMBER.toString());
	    paraMap.add("direction", Direction.ASCENDING.getDirectionCode().toString());
	    paraMap.add("page", "0");
	    paraMap.add("size", "5");
	    
	    try {
		mvc.perform(get(BASE_STORE_URL).params(paraMap))
		.andExpect(status().is5xxServerError())
		.andExpect(content().string("Invalid sort direction"));
	    } catch(Exception invalidSortOrderException) {
	    	throw (Exception) invalidSortOrderException.getCause();
	    }
				
	}
}

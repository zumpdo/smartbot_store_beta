package com.axidio.smartbot.store.utils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

import org.bson.types.ObjectId;

import com.axidio.smartbot.store.model.Store;

public class TestUtils {

	private TestUtils() {
	}

	public static Store prepareStore() {
		final Store store = new Store();
		store.setId(ObjectId.get());
		store.setName(randomAlphabetic(20));
		store.setLocation(randomAlphabetic(20));
		store.setNumber(randomNumeric(5));
		return store;
	}

}

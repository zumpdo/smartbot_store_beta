package com.axidio.smartbot.store.handlers;

import org.springframework.http.HttpStatus;

public class ApiError {

	private HttpStatus status;
    private String errorCode;
    private String message;
    private String debugMessage;
    
    public HttpStatus getStatus() {
 		return status;
 	}

 	public void setStatus(HttpStatus status) {
 		this.status = status;
 	}

 	public static final class ResponseBuilder {
        private HttpStatus status;
        private String errorCode;
        private String message;
        private String debugMessage;

        private ResponseBuilder() {
        }

        public static ResponseBuilder anApiErrorResponse() {
            return new ResponseBuilder();
        }

        public ResponseBuilder withStatus(HttpStatus status) {
            this.status = status;
            return this;
        }

        public ResponseBuilder withErrorCode(String error_code) {
            this.errorCode = error_code;
            return this;
        }

        public ResponseBuilder withMessage(String message) {
            this.message = message;
            return this;
        }

        public ResponseBuilder withDebugMessage(String detail) {
            this.debugMessage = detail;
            return this;
        }

        public ApiError build() {
            ApiError apiErrorResponse = new ApiError();
            apiErrorResponse.status = this.status;
            apiErrorResponse.errorCode = this.errorCode;
            apiErrorResponse.debugMessage = this.debugMessage;
            apiErrorResponse.message = this.message;
            return apiErrorResponse;
        }
    }

	@Override
	public String toString() {
		return "ApiError [status=" + status + ", error_code=" + errorCode + ", message=" + message + ", debugMessage="
				+ debugMessage + "]";
	}
	
 	
}

package com.axidio.smartbot.store.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.axidio.smartbot.store.exceptions.StoreNotFoundException;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {



	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return errorResponse(HttpStatus.BAD_REQUEST, "Invalid JSON");
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(StoreNotFoundException.class)
	protected ResponseEntity<Object> handleNoSuchElementException(Exception ex) {
		
//		return errorResponse(HttpStatus.NOT_FOUND, "Store not found - " + ex.getLocalizedMessage());
		
		return errorResponse(ApiError.ResponseBuilder
				.anApiErrorResponse().withStatus(HttpStatus.NOT_FOUND).withMessage("Store not found.").withDebugMessage(ex.getMessage()).build());
	}

	private ResponseEntity<Object> errorResponse(HttpStatus status, String message) {
		return ResponseEntity.status(status).body(message);
	}
	
	private ResponseEntity<Object> errorResponse(ApiError apiError) {
		return new ResponseEntity<>(apiError.toString(), apiError.getStatus());
	}
}

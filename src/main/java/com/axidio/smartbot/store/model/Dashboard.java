/**
 * 
 */
package com.axidio.smartbot.store.model;

import java.util.List;

/**
 * @author Subash
 *
 */
public class Dashboard implements Panel {
	
	private List<Component> components;

	/**
	 * @return the components
	 */
	public List<Component> getComponents() {
		return components;
	}

	/**
	 * @param components the components to set
	 */
	public void setComponents(List<Component> components) {
		this.components = components;
	}

}

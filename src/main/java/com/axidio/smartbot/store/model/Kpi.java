package com.axidio.smartbot.store.model;

public interface Kpi {
	
	public boolean isUp();
	
	public String getCode();
	
	public String getMessage();
	
	public boolean getFlag();

}

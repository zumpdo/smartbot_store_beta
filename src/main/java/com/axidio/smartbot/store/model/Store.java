package com.axidio.smartbot.store.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a TJX store.")
public class Store {
	
	@Id
	@ApiModelProperty(notes = "Unique identifier of a store. No two stores can have the same id.", example = "das87d8as7das7d1", required = true, position = 0)
	private ObjectId id;
	
	@ApiModelProperty(notes = "Location of a store. ", example = "Miami, FL", required = true, position = 0)
	private String location;
	
	@ApiModelProperty(notes = "Number of departments inside a store. ", example = "20", required = true, position = 0)
	private int departments;
	
	@ApiModelProperty(notes = "Name of the store. ", example = "TJX California", required = true, position = 0)
	private String name;
	

	@ApiModelProperty(notes = "Store number. ", example = "T897", required = true, position = 0)
	private String number;
	
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}

	// ObjectId needs to be converted to string
	public String getId() {
		return id.toHexString();
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getDepartments() {
		return departments;
	}
	public void setDepartments(int departments) {
		this.departments = departments;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Store [id=" + id + ", location=" + location + ", departments=" + departments + ", name=" + name
				+ ", number=" + number + "]";
	}
	
	
}

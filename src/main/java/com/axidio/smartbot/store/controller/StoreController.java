package com.axidio.smartbot.store.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.axidio.smartbot.store.enums.Direction;
import com.axidio.smartbot.store.enums.OrderBy;
import com.axidio.smartbot.store.exceptions.InvalidSortOrderException;
import com.axidio.smartbot.store.exceptions.StoreNotFoundException;
import com.axidio.smartbot.store.model.Store;
import com.axidio.smartbot.store.service.StoresService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/stores")
@Api(tags = "Stores API")
public class StoreController {

	@Autowired
	private StoresService service;

	/**
	 * Return json data for the stores based on the param data
	 */

	@GetMapping(value = "/", params = { "orderBy", "direction", "page", "size" })
	@ApiOperation("Returns list of all stores in the system.")
	public Page<Store> getStores(@RequestParam("orderBy") String orderBy, @RequestParam("direction") String direction,
			@RequestParam("page") int page, @RequestParam("size") int size) {

		if (!(direction.equals(Direction.ASCENDING.getDirectionCode())
				|| direction.equals(Direction.DESCENDING.getDirectionCode()))) {
			throw new InvalidSortOrderException("Invalid sort direction");
		}
		if (!(orderBy.equals(OrderBy.NUMBER.getOrderByCode())
				|| orderBy.equals(OrderBy.DEPARTMENTS.getOrderByCode()))) {
			throw new InvalidSortOrderException("Invalid orderBy condition");
		}

		return service.getStores(orderBy, direction, page, size);
	}

	/**
	 * Return json information about a single store based on the id
	 */

	@GetMapping(value = "/{id}")
	@ApiOperation("Returns a store based on id")
	public Store getStoreById(@PathVariable("id") ObjectId id) {
		Optional<Store> store = service.getStoreById(id);

		if (store.isPresent())
			return store.get();
		else
			throw new StoreNotFoundException("Store not found with id " + id);
	}

	/**
	 * Return json information about a single store based on number
	 */

	@GetMapping(value = "/get/{number}")
	@ApiOperation("Returns a store based on store number")
	public Store getStoreByNumber(@PathVariable("number") String number) {
		Optional<Store> store = service.getStoreByNumber(number);

		if (store.isPresent())
			return store.get();
		else
			throw new StoreNotFoundException("Store not found with number " + number);
	}

	/**
	 * Updates Store entity in database. Based on id.
	 */

	@PutMapping(value = "/{id}")
	@ApiOperation("Updates a store based on id")
	public Store modifyStoreById(@PathVariable("id") ObjectId id, @Valid @RequestBody Store store) {
		return service.modifyStoreById(id, store);
	}

	/**
	 * Creates a new Store entity.
	 */
	@PostMapping(value = "/")
	@ApiOperation("Creates a new store")
	public Store createStore(@Valid @RequestBody Store store) {
		return service.createStore(store);
	}

	/**
	 * Deletes a store entity.
	 */
	@DeleteMapping(value = "/{id}")
	@ApiOperation("Deletes a store based on id")
	public void deleteStore(@PathVariable ObjectId id) {
		service.deleteStore(id);
	}
}

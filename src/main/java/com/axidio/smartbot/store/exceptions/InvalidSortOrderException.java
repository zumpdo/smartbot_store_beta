package com.axidio.smartbot.store.exceptions;

public class InvalidSortOrderException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6916427170090850209L;
	private final String errorMessage;

	@Override
	public String getMessage() {
		return errorMessage;
	}

	public InvalidSortOrderException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
}
package com.axidio.smartbot.store.service;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.axidio.smartbot.store.exceptions.StoreNotFoundException;
import com.axidio.smartbot.store.model.Store;
import com.axidio.smartbot.store.repository.StoresRepository;

@Service
public class StoresService implements IStoresService {

	@Autowired
	private StoresRepository repository;

	/**
	 * Return stores data based on the param data
	 */

	public Page<Store> getStores(String orderBy, String direction, int page, int size) {
		Pageable pageable = PageRequest.of(page, size,
				"ASC".equals(direction) ? Sort.by(orderBy).ascending() : Sort.by(orderBy).descending());
		return repository.findAll(pageable);
	}

	/**
	 * Find a store by id
	 */

	public Optional<Store> getStoreById(ObjectId id) {
		return repository.findById(id);
	}

	/**
	 * Update a store by id
	 */

	public Store modifyStoreById(ObjectId id, Store store) {
		store.setId(id);
		repository.save(store);
		Optional<Store> savedStore = repository.findById(id);

		if (savedStore.isPresent())
			return savedStore.get();
		else
			throw new StoreNotFoundException("Store not found with id " + id);
	};

	/**
	 * Creates a new store by id
	 */
	public Store createStore(Store store) {
		store.setId(ObjectId.get());
		repository.save(store);
		return store;
	}

	/**
	 * Deletes a store by id
	 */
	public void deleteStore(ObjectId id) {
		Optional<Store> store = repository.findById(id);
		if (store.isPresent())
			repository.delete(store.get());
	}

	/**
	 * Find a store by number
	 */
	public Optional<Store> getStoreByNumber(String number) {
		return repository.findByNumber(number);
	}

	/**
	 * Deletes all stores
	 */
	public void deleteAll() {
		repository.deleteAll();
	}

}

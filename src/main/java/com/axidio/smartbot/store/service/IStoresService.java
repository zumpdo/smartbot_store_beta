package com.axidio.smartbot.store.service;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;

import com.axidio.smartbot.store.model.Store;

/**
 * @author Subash
 * 
 * Contract for StoresService
 *
 */
public interface IStoresService {
	
	public Page<Store> getStores(String orderBy, String direction, int page, int size);
	
	public Optional<Store> getStoreById(ObjectId id);
	  
	  public Store modifyStoreById(ObjectId id, Store store);
	  
	  public Store createStore(Store store);
	  
	  public void deleteStore(ObjectId id);
	  
	  public Optional<Store> getStoreByNumber(String number);

}

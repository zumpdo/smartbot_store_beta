package com.axidio.smartbot.store.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class Swagger2Config {
	
    @Bean
    public Docket storesApi() {
        return new Docket(DocumentationType.SWAGGER_2) //.groupName("V1")
        		.select()
            .apis(RequestHandlerSelectors
                .basePackage("com.axidio.smartbot.store.controller"))
            .paths(PathSelectors.regex("/.*"))
            .build().apiInfo(apiEndPointsInfo()).tags(new Tag("Stores API", "Set of endpoints for Creating, Retrieving, Updating and Deleting of Stores."));
    }
    
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("SmartBot Stores API Documentation")
            .description("SmartBot REST API")
            .contact(new Contact("Eric Cheng", "www.axidio.com", "echeng@axidio.com"))
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.2")
            .build();
    }
}
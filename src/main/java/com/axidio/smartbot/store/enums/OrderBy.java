package com.axidio.smartbot.store.enums;

public enum OrderBy {
	NUMBER("number"), DEPARTMENTS("departments");
	private String orderByCode;

	private OrderBy(String orderBy) {
		this.orderByCode = orderBy;
	}

	public String getOrderByCode() {
		return this.orderByCode;
	}
}

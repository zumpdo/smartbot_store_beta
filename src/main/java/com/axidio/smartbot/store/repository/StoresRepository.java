package com.axidio.smartbot.store.repository;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.axidio.smartbot.store.model.Store;

@Repository
public interface StoresRepository extends MongoRepository<Store, String>, PagingAndSortingRepository<Store, String> {
		Optional<Store> findById(ObjectId id);
		Optional<Store> findByNumber(String number);
}
